# Device Service

Prerequisite software to build, run, and testing this service are:
* Java JDK 11 or newer
* Apache Maven 3.6 or newer
* Docker 19  or newer
* Kubernetes 1.17 or newer

For Docker and Kubernetes, you can use Docker Desktop 3.0.0 or newer (Windows or Mac).

## Build, Test, and Package
To compile, run unit and integration tests, and create the executable jar, run the following commands.
NOTE: PostgreSQL (postgres) is needed for integration tests. 
```shell
docker run --name postgres-it -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres:11
mvn clean verify
docker rm -f postgres-it
```

## Dockerize
To create a Docker image of the application, including any/all runtime dependencies, run the following Docker command.
```shell
docker build -t bartman111/device-service:0.1.0 .
```

## Kubernetes Deploy
To deploy the application as a microservice, run the following Kubernetes `kubectl` command
```shell
kubectl apply -f k8s.yaml
```