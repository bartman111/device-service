package com.ingleston.demo.deviceservice;

import com.ingleston.demo.deviceservice.exception.DeviceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class DeviceService {

    private final DeviceRepository deviceRepository;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Transactional(readOnly = true)
    public List<Device> getDeviceByType(String type) {
        return deviceRepository.findByType(type);
    }

    @Transactional(readOnly = true)
    public List<Device> getAllDevices() {
        return deviceRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Device> getDeviceById(UUID id) {
        return deviceRepository.findById(id);
    }

    @Transactional
    public Device updateDeviceStatusById(UUID id, Status newStatus) throws DeviceNotFoundException{
        final Device device = deviceRepository.findById(id).orElseThrow(DeviceNotFoundException::new);

        final Action newAction = this.getActionFromStatus(newStatus);

        device.setStatus(newStatus);
        device.setAction(newAction);

        return device;
    }

    private Action getActionFromStatus(Status status) {
        Action action;
        switch (status) {
            case STOPPED:
                action = Action.START;
                break;
            case FATAL:
                action = Action.STOP;
                break;
            case ERROR, UNKNOWN:
                action = Action.RESTART;
                break;
            case RUNNING:
            default:
                action = Action.NONE;
        }
        return action;
    }
}

