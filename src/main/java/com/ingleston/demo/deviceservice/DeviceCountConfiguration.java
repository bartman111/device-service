package com.ingleston.demo.deviceservice;

import java.util.List;

public class DeviceCountConfiguration {
    private List<List<String>> connections;

    private List<String> toggleIps;

    public List<List<String>> getConnections() {
        return connections;
    }

    public void setConnections(List<List<String>> connections) {
        this.connections = connections;
    }

    public List<String> getToggleIps() {
        return toggleIps;
    }

    public void setToggleIps(List<String> toggleIps) {
        this.toggleIps = toggleIps;
    }
}
