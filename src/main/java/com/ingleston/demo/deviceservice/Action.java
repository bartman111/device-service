package com.ingleston.demo.deviceservice;

public enum Action {
    START,
    STOP,
    RESTART,
    NONE
}
