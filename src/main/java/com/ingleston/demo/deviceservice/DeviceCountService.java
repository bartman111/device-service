package com.ingleston.demo.deviceservice;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.IntStream;

@Service
public class DeviceCountService {
    public List<Integer> getNumberOfDevices(DeviceCountConfiguration deviceCountConfiguration) {
        final List<List<String>> connections = deviceCountConfiguration.getConnections();
        final List<String> toggleIps = deviceCountConfiguration.getToggleIps();

        List<Integer> numberOfDevicesAffected = new ArrayList<>(toggleIps.size());
        IntStream.range(0, toggleIps.size()).forEach(index -> numberOfDevicesAffected.add(0));
        List<Boolean> currentDeviceStateArray = new ArrayList<>(connections.size());
        IntStream.range(0, toggleIps.size()).forEach(index -> currentDeviceStateArray.add(false));
        //short-cut, return 0's
        if (connections.isEmpty() || toggleIps.size() < 2) {
            return numberOfDevicesAffected;
        }

        //Create hashmap of ips and their intial state, will toggle later
        HashMap<String, Boolean> ipStates = new HashMap<>();
        for (List<String> connection : connections) {
            IntStream.range(0, connection.size()).forEach(ipIndex -> ipStates.put(connection.get(ipIndex), false));
        }

        for(int toggleIpsIndex = 0; toggleIpsIndex < toggleIps.size(); toggleIpsIndex++) {
            String toggleIp = toggleIps.get(toggleIpsIndex);

            if(ipStates.get(toggleIp) != null) {
                boolean currentIpState = ipStates.get(toggleIp);
                boolean newIpState = !currentIpState;
                ipStates.put(toggleIp, newIpState);

                for(int deviceIndex = 0; deviceIndex < connections.size(); deviceIndex++) {
                    boolean newDeviceState = ipStates.get(connections.get(deviceIndex).get(1)) && ipStates.get(connections.get(deviceIndex).get(0));
                    if(newDeviceState != currentDeviceStateArray.get(deviceIndex)) {
                        currentDeviceStateArray.set(deviceIndex, newDeviceState);
                        numberOfDevicesAffected.set(toggleIpsIndex, numberOfDevicesAffected.get(toggleIpsIndex) + 1);
                    }
                }
            }
        }

        return numberOfDevicesAffected;
    }
}
