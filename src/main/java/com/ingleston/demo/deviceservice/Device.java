package com.ingleston.demo.deviceservice;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.UUID;

import static javax.persistence.EnumType.STRING;

@Entity
@Table(name = "device")
public class Device {

    @Id
    private UUID id;

    private String type;

    @NotNull
    @Enumerated(value = STRING)
    private Status status;

    @NotNull
    @Enumerated(value = STRING)
    private Action action;

    public Device(UUID id, String type, Status status, Action action) {
        this.id = id;
        this.type = type;
        this.status = status;
        this.action = action;
    }

    public Device() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setType(String name) {
        this.type = name;
    }

    public String getType() {
        return type;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

}
