package com.ingleston.demo.deviceservice;

public enum Status {
    RUNNING,
    STOPPED,
    ERROR,
    FATAL,
    UNKNOWN
}
