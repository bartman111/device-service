package com.ingleston.demo.deviceservice;

import com.ingleston.demo.deviceservice.exception.DeviceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/devices")
@CrossOrigin
public class DeviceController {

    private final DeviceService deviceService;

    @Autowired
    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @GetMapping
    public List<Device> getDeviceStatusByOptionalType(@RequestParam(required = false) String type) {
        if(type != null) {
            return deviceService.getDeviceByType(type);
        } else {
            return deviceService.getAllDevices();
        }
    }

    @GetMapping("/{id}")
    public Device getDeviceById(@PathVariable @Valid UUID id) {
        return deviceService.getDeviceById(id).orElseThrow(DeviceNotFoundException::new);
    }

    @GetMapping("/{id}/status")
    public String getDeviceStatusById(@PathVariable @Valid UUID id) throws DeviceNotFoundException {
        final Optional<Device> optional = deviceService.getDeviceById(id);
        if(optional.isEmpty()) {
            throw new DeviceNotFoundException();
        }
        return optional.get().getStatus().name();

    }

    @PutMapping("/{id}/status")
    public Device updateDeviceStatusById(@PathVariable @Valid UUID id, @RequestBody Status status) throws  DeviceNotFoundException{
        return deviceService.updateDeviceStatusById(id, status);
    }
}
