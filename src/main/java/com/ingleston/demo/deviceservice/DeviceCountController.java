package com.ingleston.demo.deviceservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController()
@RequestMapping("/api/devices")
public class DeviceCountController {

    private final DeviceCountService deviceCountService;

    @Autowired
    public DeviceCountController(DeviceCountService deviceCountService) {
        this.deviceCountService = deviceCountService;
    }

    @PostMapping(value = "/count")
    public List<Integer> countDeviceBlah(@RequestBody @Valid DeviceCountConfiguration deviceCountConfiguration) {
        return deviceCountService.getNumberOfDevices(deviceCountConfiguration);
    }
}
