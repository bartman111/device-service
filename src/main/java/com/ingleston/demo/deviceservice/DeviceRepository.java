package com.ingleston.demo.deviceservice;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface DeviceRepository extends CrudRepository<Device, UUID> {
    List<Device> findByType(String name);
    Device findById(Integer id);
    List<Device> findAll();
}
