CREATE TABLE IF NOT EXISTS device (
    id UUID PRIMARY KEY NOT NULL,
    type varchar(255),
    status varchar(255),
    action varchar(255)
);