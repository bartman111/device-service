package com.ingleston.demo.deviceservice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@SpringBootTest
@DirtiesContext
@AutoConfigureTestEntityManager
@Transactional
class DeviceServiceIT {


    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private DeviceService deviceService;

    @Test
    void getsDeviceByType() {
        final List<Device> deviceList = deviceService.getDeviceByType("switch");
        assertThat(deviceList.size(), is(1));
    }

    @Test
    void getsAllDevices() {
        final List<Device> deviceList = deviceService.getAllDevices();
        assertThat(deviceList.size(), is(4));
    }

    @Test
    void getsDeviceById() {
        final Device device = deviceService.getDeviceById(UUID.fromString("ea255454-4538-11eb-b378-0242ac130002")).get();
        assertThat(device.getId().toString(), is("ea255454-4538-11eb-b378-0242ac130002"));
        assertThat(device.getType(), is("switch"));
        assertThat(device.getStatus(), is(Status.UNKNOWN));
        assertThat(device.getAction(), is(Action.RESTART));
    }

    @Test
    void getDeviceByIdReturnsEmptyOptional() {
        final Optional<Device> device = deviceService.getDeviceById(UUID.fromString("ea255454-4538-11eb-b378-0242ac130002"));
        assertThat(device.isEmpty(), is(false));
    }

    @Test
    void updateDeviceStatusById() {
        Device device = deviceService.updateDeviceStatusById(UUID.fromString("ea2551ac-4538-11eb-b378-0242ac130002"), Status.STOPPED);
        assertThat(device.getStatus(), is(Status.STOPPED));
        assertThat(device.getAction(), is(Action.START));

        device = deviceService.updateDeviceStatusById(UUID.fromString("ea2551ac-4538-11eb-b378-0242ac130002"), Status.FATAL);
        assertThat(device.getStatus(), is(Status.FATAL));
        assertThat(device.getAction(), is(Action.STOP));

        device = deviceService.updateDeviceStatusById(UUID.fromString("ea2551ac-4538-11eb-b378-0242ac130002"), Status.ERROR);
        assertThat(device.getStatus(), is(Status.ERROR));
        assertThat(device.getAction(), is(Action.RESTART));

        device = deviceService.updateDeviceStatusById(UUID.fromString("ea2551ac-4538-11eb-b378-0242ac130002"), Status.UNKNOWN);
        assertThat(device.getStatus(), is(Status.UNKNOWN));
        assertThat(device.getAction(), is(Action.RESTART));

        device = deviceService.updateDeviceStatusById(UUID.fromString("ea2551ac-4538-11eb-b378-0242ac130002"), Status.RUNNING);
        assertThat(device.getStatus(), is(Status.RUNNING));
        assertThat(device.getAction(), is(Action.NONE));
    }

}