package com.ingleston.demo.deviceservice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DeviceServiceApplicationIT {

    @Autowired
    private DeviceCountController deviceCountController;

    @Test
    void contextLoads() {
        assertThat(deviceCountController).isNotNull();
    }
}
