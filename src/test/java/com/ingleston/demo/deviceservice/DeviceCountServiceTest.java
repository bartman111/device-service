package com.ingleston.demo.deviceservice;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class DeviceCountServiceTest {

    @Test
    void getsNumberOfDevices1() {
        List<List<String>> connections = Arrays.asList(
            Arrays.asList("207.21.18.5","29.189.54.235"),
			Arrays.asList("29.189.54.235","164.130.238.46"),
			Arrays.asList("211.26.233.240","29.189.54.235"),
			Arrays.asList("110.38.64.92","29.189.54.235"),
			Arrays.asList("32.1.192.77","29.189.54.235"),
			Arrays.asList("29.189.54.235","150.156.230.47"),
			Arrays.asList("29.189.54.235","138.227.165.73"),
			Arrays.asList("96.133.252.137","29.189.54.235"),
			Arrays.asList("29.189.54.235","53.194.195.26"),
			Arrays.asList("29.189.54.235","134.227.189.89")
        );

        List<String> toggleIps = Arrays.asList("150.156.230.47",
                "29.189.54.235",
                "29.189.54.235",
                "202.41.195.148",
                "29.189.54.235",
                "29.189.54.235",
                "29.189.54.235",
                "183.143.225.73",
                "201.23.55.210",
                "29.189.54.235");

        final DeviceCountConfiguration deviceCountConfiguration = new DeviceCountConfiguration();
        deviceCountConfiguration.setConnections(connections);
        deviceCountConfiguration.setToggleIps(toggleIps);
        DeviceCountService deviceCountService = new DeviceCountService();
        List<Integer> devicesAffected = deviceCountService.getNumberOfDevices(deviceCountConfiguration);
        List<Integer> expectedDevicesAffected = Arrays.asList(0, 1, 1, 0, 1, 1, 1, 0, 0, 1);
        assertThat(devicesAffected).isEqualTo(expectedDevicesAffected);
    }

    @Test
    void getsNumberOfDevices2() {
        List<List<String>> connections = Collections.singletonList(
                Arrays.asList("165.192.233.156", "54.199.242.131")
        );

        List<String> toggleIps = Arrays.asList("63.10.176.235",
                "165.192.233.156",
                "63.10.176.235",
                "63.10.176.235",
                "165.192.233.156",
                "63.10.176.235",
                "165.192.233.156",
                "54.199.242.131",
                "165.192.233.156",
                "165.192.233.156");

        final DeviceCountConfiguration deviceCountConfiguration = new DeviceCountConfiguration();
        deviceCountConfiguration.setConnections(connections);
        deviceCountConfiguration.setToggleIps(toggleIps);
        DeviceCountService deviceCountService = new DeviceCountService();
        List<Integer> devicesAffected = deviceCountService.getNumberOfDevices(deviceCountConfiguration);
        List<Integer> expectedDevicesAffected = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 1, 1, 1);
        assertThat(devicesAffected).isEqualTo(expectedDevicesAffected);
    }
}