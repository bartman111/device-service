package com.ingleston.demo.deviceservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DeviceCountController.class)
class DeviceCountControllerIT {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DeviceCountService mockDeviceCountService;

    @Test
    void calculatesDeviceCount() throws Exception {
        DeviceCountConfiguration deviceCountConfiguration = new DeviceCountConfiguration();
        deviceCountConfiguration.setConnections(Arrays.asList(
                Arrays.asList("207.21.18.5","29.189.54.235"),
                Arrays.asList("29.189.54.235","164.130.238.46")
            ));
        deviceCountConfiguration.setToggleIps(Collections.singletonList("10.0.54.234"));

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(deviceCountConfiguration);

        List<Integer> results = Collections.singletonList(0);

        given(mockDeviceCountService.getNumberOfDevices(any())).willReturn(results);
        this.mockMvc.perform(post("/api/devices/count")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[0]"));

    }

    @Test
    void calculateDeviceCountReturns400() throws Exception {
        this.mockMvc.perform(post("/api/devices/count")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"connections\": \"foo\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}