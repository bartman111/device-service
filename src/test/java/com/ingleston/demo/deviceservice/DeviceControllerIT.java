package com.ingleston.demo.deviceservice;

import com.ingleston.demo.deviceservice.exception.DeviceNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DeviceController.class)
class DeviceControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DeviceService mockDeviceService;

    @Test
    void getsDevice() throws Exception {
        final Device device = new Device(UUID.fromString("ea2551ac-4538-11eb-b378-0242ac130002"),
                "router",
                Status.RUNNING,
                Action.NONE);
        given(mockDeviceService.getAllDevices()).willReturn(Collections.singletonList(device));
        this.mockMvc.perform(get("/api/devices"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(String.format("[{\"id\":\"%s\",\"type\":\"%s\",\"status\":\"%s\",\"action\":\"%s\"}]",
                        device.getId(),
                        device.getType(),
                        device.getStatus(),
                        device.getAction())));
    }

    @Test
    void getDeviceById() throws Exception {
        final UUID uuid = UUID.fromString("ea25554e-4538-11eb-b378-0242ac130002");
        final Device device = new Device(uuid,
                "router",
                Status.RUNNING,
                Action.NONE);
        given(mockDeviceService.getDeviceById(uuid)).willReturn(Optional.of(device));
        this.mockMvc.perform(get(String.format("/api/devices/%s", uuid.toString())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(String.format("{\"id\":\"%s\",\"type\":\"%s\",\"status\":\"%s\",\"action\":\"%s\"}",
                        device.getId(),
                        device.getType(),
                        device.getStatus(),
                        device.getAction())));
    }

    @Test
    void getDeviceWithBadUUIDReturns400() throws Exception {
        this.mockMvc.perform(get("/api/devices/asdfasdfasdfa"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void getsDeviceByOptionalType() throws Exception {
        final Device device1 = new Device(UUID.fromString("ea2551ac-4538-11eb-b378-0242ac130002"),
                "router",
                Status.RUNNING,
                Action.NONE);
        final Device device2 = new Device(UUID.fromString("ea2551ac-1111-11eb-b378-0242ac139999"),
                "router",
                Status.RUNNING,
                Action.NONE);
        given(mockDeviceService.getDeviceByType("router")).willReturn(Arrays.asList(device1, device2));

        this.mockMvc.perform(get("/api/devices?type=router"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(String.format("[{\"id\":\"%s\",\"type\":\"%s\",\"status\":\"%s\",\"action\":\"%s\"}," +
                                "{\"id\":\"%s\",\"type\":\"%s\",\"status\":\"%s\",\"action\":\"%s\"}]",
                        device1.getId(),
                        device1.getType(),
                        device1.getStatus(),
                        device1.getAction(),
                        device2.getId(),
                        device2.getType(),
                        device2.getStatus(),
                        device2.getAction())));
    }

    @Test
    void getDeviceStatusById() throws Exception {
        final UUID uuid = UUID.fromString("ea25554e-4538-11eb-b378-0242ac130002");
        final Device device = new Device(uuid,
                "router",
                Status.RUNNING,
                Action.NONE);
        given(mockDeviceService.getDeviceById(uuid)).willReturn(Optional.of(device));
        this.mockMvc.perform(get(String.format("/api/devices/%s/status", uuid.toString())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(String.format("%s", device.getStatus().name())));
    }

    @Test
    void getDeviceStatusByIdReturns404() throws Exception {
        given(mockDeviceService.getDeviceById(UUID.fromString("ea2551ac-4538-11eb-b378-0242ac13999"))).willThrow(new DeviceNotFoundException());
        this.mockMvc.perform(get("/api/devices/ea2551ac-4538-11eb-b378-0242ac139999/status"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void  updateDeviceStatusById() throws Exception {
        final UUID uuid = UUID.fromString("ea25554e-4538-11eb-b378-0242ac130002");
        final Device device = new Device(uuid,
                "router",
                Status.ERROR,
                Action.RESTART);
        given(mockDeviceService.updateDeviceStatusById(uuid, Status.ERROR)).willReturn(device);
        this.mockMvc.perform(put(String.format("/api/devices/%s/status", uuid.toString()))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(String.format("\"%s\"", Status.ERROR.name())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(String.format("{\"id\":\"%s\",\"type\":\"%s\",\"status\":\"%s\",\"action\":\"%s\"}",
                        device.getId(),
                        device.getType(),
                        device.getStatus(),
                        device.getAction())));
    }

    @Test
    void  updateDeviceStatusByIdReturns400() throws Exception {
        this.mockMvc.perform(put("/api/devices/asdfasdfasdfa/status").content(Status.ERROR.name()))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}